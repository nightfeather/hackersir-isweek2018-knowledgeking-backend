import Game from '../lib/game';
import Player from '../lib/player';
import { makeHelpers, validateData } from './helpers';

const egg = '満開STEP BY STEPで進め! STEP OUT さぁ、飛び出せ! ワタシ色 開花最前線、上昇中〜♪';

function start(socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'start');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }
}

function cancel(socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'cancel');
}

function getProblem(socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'getProblem');
}

function answer(socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'answer');
}

export default {
  start,
  cancel,
  getProblem,
  answer,
};
