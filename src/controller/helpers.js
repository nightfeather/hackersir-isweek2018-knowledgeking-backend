const makeHelpers = function (self, socket, cmd) {
  const tResult = function (sock, c, ...data) {
    socket.emit(cmd, Object.assign({}, ...data));
  };

  const response = tResult.bind(self, socket, cmd);
  const success = response.bind(self, { ok: true });
  const failure = response.bind(self, { ok: false });

  return [response, success, failure];
};

/* eslint-disable valid-typeof */
const validateData = function (data, fields) {
  const result = [];
  const dataFields = Object.keys(data || {});
  Object.entries(fields).forEach((v) => {
    const [f, t] = v;
    if (!dataFields.includes(f)) {
      result.push(`${f} is empty`);
    } else if (!(typeof data[f] === t)) {
      result.push(`${f} is wrong type `);
    }
  });
  return result;
};
/* eslint-enable valid-typeof */

export { makeHelpers, validateData };
