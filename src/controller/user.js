import mongoose from 'mongoose';
import { makeHelpers, validateData } from './helpers';
import Player from '../lib/player';
import { redisGetAsync } from '../lib/redis_helpers';

const User = mongoose.model('User');

const login = function (socket, data) {
  const [response, success, failure] = makeHelpers(this, socket, 'login');

  const validation = validateData(data, { username: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.findSession(socket).then((res) => {
    if (res) {
      const obj = { mesg: 'already logged in' };
      throw (obj);
    }

    return User.collection.findAndModify(
      { username: data.username },
      [],
      { $setOnInsert: { username: data.username } },
      {
        new: true, // return new doc if one is upserted
        upsert: true, // insert the document if it does not exist
      },
    );
  }).then((res) => {
    if (res.ok === 0) { throw (res.lastErrorObject); }

    return User.findById(res.value).then(async (user) => {
      let err;
      if (await redisGetAsync(user.id)) {
        err = { mesg: 'already logged in somewhere' };
        throw err;
      }
      const player = new Player(user, socket);
      player.save();

      if (user.nickname) {
        success({ mesg: 'ok', token: player.id });
      } else {
        success({ mesg: 'set your nickname plz', registered: false, token: player.id });
      }
    });
  }).catch((error) => {
    let err = error;
    if (!error.mesg) { err = { mesg: error }; }
    failure(err);
  });
};

const setName = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'setName');

  const validation = validateData(data, { token: 'string', nickname: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    let err;
    if (!player) {
      err = { mesg: 'invalid token' };
      throw (err);
    }
    const { user } = player;
    if (user) {
      if (!user.nickname) {
        user.nickname = data.nickname;
        return user.save();
      }
      err = { mesg: 'already have nickname' };
      throw (err);
    } else {
      err = { mesg: 'invalid user' };
      throw (err);
    }
  }).then((res) => {
    success({ nickname: res.nickname });
  }).catch((error) => {
    let err = error;
    if (!error.mesg) { err = { mesg: error }; }
    failure(err);
  });
};

const getName = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'getName');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    let err;
    if (!player) {
      err = ({ mesg: 'invalid token' });
      throw err;
    }
    const { user } = player;
    if (user) {
      success({ nickname: user.nickname });
    } else {
      err = { mesg: 'invalid user' };
      throw err;
    }
  }).catch((error) => {
    let err = error;
    if (!error.mesg) { err = { mesg: error }; }
    failure(err);
  });
};

const getScore = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'getScore');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    let err;

    if (!player) {
      err = { mesg: 'invalid token' };
      throw err;
    }

    const { user } = player;
    if (user) {
      success({ score: user.score });
    } else {
      err = { mesg: 'invalid user' };
      throw err;
    }
  }).catch((error) => {
    let err = error;
    if (!error.mesg) { err = { mesg: error }; }
    failure(err);
  });
};

const logout = function (socket, data) {
  const [, success, failure] = makeHelpers(this, socket, 'logout');

  const validation = validateData(data, { token: 'string' });
  if (validation.length > 0) {
    failure({ mesg: 'some errors in input', errors: validation });
    return;
  }

  Player.load(data.token, socket).then((player) => {
    let err;
    if (!player) {
      err = { mesg: 'invalid token' };
      throw err;
    }
    return player.destroy();
  }).then(() => {
    success();
  }).catch((error) => {
    let err = error;
    if (!error.mesg) { err = { mesg: error }; }
    failure(err);
  });
};

export default {
  login,
  setName,
  getName,
  getScore,
  logout,
};
