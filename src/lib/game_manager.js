import Game from './game';

class GameManager {
  constructor() {
    this.games = [];
    this.player_queue = [];
  }

  wait(player) {
    this.player_queue.push(player);
  }

  match(player) {
    // no match when queue empty, maybe need to consider race condition?
    if (this.player_queue.length <= 0) { return false; }

    // get an live player connection
    let opp = null;
    do {
      opp = this.player_queue.shift();
    } while (opp && !opp.socket.connected);
    if (opp === undefined || !opp.socket.connected) { return false; }

    const game = new Game(player, opp);
    return game;
  }

  static bot(game) {
    game.addPlayer();
  }
}

export default GameManager;
