import redis from 'redis';
import config from '../config';

async function redisGetAsync(key) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).get(key, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

async function redisSetAsync(key, val) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).set(key, val, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

async function redisDelAsync(key) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).del(key, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

async function redisLPushAsync(key, val) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).lpush(key, val, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

async function redisLLenAsync(key) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).llen(key, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

async function redisLPopAsync(key) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).lpop(key, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

async function redisExpireAsync(key, val) {
  return new Promise((resolv, reject) => {
    redis.createClient(config.redisURI).expire(key, val, (err, res) => {
      if (err) { reject(err); } else { resolv(res); }
    });
  });
}

export {
  redisGetAsync,
  redisSetAsync,
  redisDelAsync,
  redisLPushAsync,
  redisLPopAsync,
  redisLLenAsync,
  redisExpireAsync,
};
