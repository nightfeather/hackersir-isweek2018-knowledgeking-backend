import crypto from 'crypto';
import User from '../db/User';
import {
  redisSetAsync,
  redisGetAsync,
  redisDelAsync,
  redisExpireAsync,
} from './redis_helpers';

/*
 * Player Class, or should call it Session Class
 *
 * Initiate: At user login
 * Contains: User data/id, socket id
 */
class Player {
  constructor(user, socket, id) {
    if (!id) {
      this.id = `player#${crypto.randomBytes(16).toString('hex')}`;
    } else {
      this.id = id;
    }
    this.socket = socket;
    this.user = user;
    this.state = '';
  }

  toString() {
    const obj = {};
    obj.id = this.id;
    obj.user = this.user.id;
    obj.socket = this.socket.id;
    obj.state = this.state;
    return JSON.stringify(obj);
  }

  async save() {
    return redisSetAsync(this.socket.id, this.id)
      .then(() => redisSetAsync(this.user.id, this.id))
      .then(() => redisSetAsync(this.id, this.toString()));
  }

  async graceQuit() {
    return Promise.all([redisExpireAsync(this.socket.id, 10), redisExpireAsync(this.user.id, 10)]);
  }

  async destroy() {
    return redisDelAsync(this.id)
      .then(() => redisDelAsync(this.user.id))
      .then(() => redisDelAsync(this.socket.id));
  }

  static async findSession(socket) {
    return redisGetAsync(socket.id);
  }

  static async checkUserOnline(uid) {
    return redisGetAsync(uid);
  }

  static async load(token, sock, override = true) {
    if (!token || !sock) { return false; }
    return redisGetAsync(token).then((data) => {
      if (data) {
        const obj = JSON.parse(data);
        const socket = override ? sock : sock.server.sockets.sockets[obj.socket];
        return User.findById(obj.user).then(user => new Player(user, socket, token));
      }
      return false;
    }).then((async (player) => {
      await player.save();
      return player;
    }));
  }
}

export default Player;
