import crypto from 'crypto';
import Player from './player';
import { redisSetAsync, redisGetAsync } from './redis_helpers';

class Game {
  constructor() {
    const randid = crypto.randomBytes(8).toString('hex');
    this.id = `Room#${randid}`;
    this.state = '';
    this.players = [];
  }

  addPlayer(player) {
    let res = null;
    if (this.players.length >= 2) {
      res = false;
    } else {
      this.players.push(player);
      res = this;
    }
    return res;
  }

  toString() {
    const obj = {
      id: this.id,
      state: this.state,
      players: [],
    };

    obj.players = [];
    this.players.forEach((el) => {
      obj.players.push(el.id);
    });
    return JSON.stringify(obj);
  }

  isReady() {
    return (this.players.length >= 2);
  }

  async save() {
    return redisSetAsync(this.id, this.toString());
  }

  static async load(id) {
    return redisGetAsync(id).then((res) => {
      if (res) {
        return new Promise(async (resolv) => {
          const obj = JSON.parse(res);
          const game = new Game();
          game.id = obj.id;
          await Promise.all(obj.players.map(async (el) => {
            const player = Player.load(el.id);
            game.players.push(player);
          }));
          resolv(game);
        });
      }
      return false;
    });
  }
}

export default Game;
