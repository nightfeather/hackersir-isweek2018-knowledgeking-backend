
class RateLimit {
  constructor(server, limit) {
    this.server = server;
    this.limit = limit;
  }

  checkLimitExceed() {
    return Object.values(this.server.sockets.sockets)
      .filter(cli => !cli.disconnected).length > this.limit;
  }
}

export default RateLimit;
