/* eslint-disable no-unused-vars */
import express from 'express';
import { createServer } from 'http';
import module from 'module';
import path from 'path';
import socketIO from 'socket.io';

import config from './config';
import {} from './db';
import Player from './lib/player';
import GameManager from './lib/game_manager';
import { user, game } from './controller';
import RateLimit from './lib/rate_limit';

const app = express();
const server = createServer(app);

const io = socketIO(server);

const rtl = new RateLimit(io, 100);

server.listen(3000, '0.0.0.0'); // explictly expose port

app.use(express.static(path.join('./', 'public')));
app.get('/', (req, res) => res.send('Hello'));

io.on('connection', async function (socket) {
  console.log(`client#${socket.id}`, 'connected');

  if (rtl.checkLimitExceed()) {
    socket.emit('error', { mesg: 'connection limit exceeded' });
    socket.disconnect();
    console.log(`kicked client#${socket.id}`);
    console.log('connection limit exceeded');
    return;
  }

  socket.emit('hello', { mesg: 'greetings from server' });

  socket.on('flag', () => {
    socket.emit('frog', { mesg: 'FROG{H3R3_15_7H3_FR0G}' });
  });

  socket.on('login', user.login.bind(this, socket))
    .on('setName', user.setName.bind(this, socket))
    .on('getName', user.getName.bind(this, socket))
    .on('getScore', user.getScore.bind(this, socket))
    .on('logout', user.logout.bind(this, socket));

  socket.on('start', game.start.bind(this, socket))
    .on('cancel', game.cancel.bind(this, socket))
    .on('getProblem', game.getProblem.bind(this, socket))
    .on('answer', game.answer.bind(this, socket));

  socket.on('error', (error) => {
    console.error(`client#${socket.id}`, 'encountered error:', error);
  });

  socket.on('disconnect', (data) => {
    console.info(`client#${socket.id}`, 'left!!');

    Player.findSession(socket).then((playerId) => {
      let err;
      if (!playerId) {
        err = { mesg: 'up' };
        throw err;
      } // 🤮
      return playerId;
    }).then(res => Player.load(res, socket))
      .then((player) => {
        if (player) {
          player.graceQuit();
        }
      })
      .catch(err => console.log(err));
  });
});
