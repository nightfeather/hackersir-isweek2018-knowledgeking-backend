FROM node:11.1.0-alpine
RUN echo -e 'http://dl-cdn.alpinelinux.org/alpine/edge/main\nhttp://dl-cdn.alpinelinux.org/alpine/edge/community\nhttp://dl-cdn.alpinelinux.org/alpine/edge/testing' > /etc/apk/repositories
RUN apk add --no-cache yarn
COPY . /app
WORKDIR /app
RUN yarn
RUN yarn build
CMD ["yarn", "start"]
